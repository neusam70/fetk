import numpy as np
import fetk.material


def test_material_elastic_1d():
    material = fetk.material.factory("elastic", {"E": 10.0, "nu": 0.3})
    assert np.allclose(material.E, 10.0)
    assert np.allclose(material.nu, 0.3)
    stiff = material.stiffness(0.0, ndir=1, nshr=0)
    assert np.isscalar(stiff)
    assert np.allclose(stiff, 10.0)


def test_material_elastic_2d():
    material = fetk.material.factory("elastic", {"E": 9.375, "nu": 0.25})
    assert np.allclose(material.E, 9.375)
    assert np.allclose(material.nu, 0.25)
    stiff = material.stiffness(0.0, ndir=1, nshr=0)
    assert np.isscalar(stiff)
    assert np.allclose(stiff, 9.375)
    stiff = material.stiffness(np.array([[0.0, 0.0], [0.0, 1.0], [1.0, 0.0]]), ndir=2, nshr=1)
    expected = np.array([[10.0, 2.5, 0.0], [2.5, 10.0, 0.0], [0.0, 0.0, 3.75]])
    assert np.allclose(stiff, expected)
