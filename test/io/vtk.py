import os
import pytest
import numpy as np
from fetk.element import C2D3
from fetk.io import vtk

try:
    import distmesh
    from distmesh import drectangle, distmesh2d, huniform
except ImportError:
    distmesh = None


@pytest.mark.skipif(distmesh is None, reason="distmesh not imported")
def test_write_fe_results():
    np.random.seed(190)  # Always the same results
    fd = lambda p: drectangle(p, -1, 1, -1, 1)
    fh = huniform
    coord, elecon = distmesh2d(
        fd, fh, 0.1, (-1, -1, 1, 1), [(-1, -1), (-1, 1), (1, -1), (1, 1)]
    )
    jobid = "Job"
    nodlab = range(coord.shape[0])
    nodmap = dict([(n, n) for n in nodlab])
    elelab = range(elecon.shape[0])
    elemap = dict([(n, n) for n in elelab])
    eletyp = [C2D3] * elecon.shape[0]
    scal = np.random.rand(coord.shape[0])
    vect = np.random.rand(coord.shape[0] * 2).reshape(-1, 2)
    tens = np.random.rand(elecon.shape[0] * 9).reshape(-1, 9)
    symt = np.random.rand(elecon.shape[0] * 6).reshape(-1, 6)
    kwds = dict(scal=scal, vect=vect, tens=tens, symt=symt)
    disp = np.zeros_like(coord)
    disp[:, 0] = 1
    vtk.snapshot(jobid, coord, nodmap, elemap, eletyp, elecon, disp=disp, **kwds)
    filename = jobid + ".vtu"
    vtk.write_vtu_mesh(filename, coord, nodlab, elelab, eletyp, elecon, check=1)
    os.remove(filename)


if __name__ == "__main__":
    # ReadMesh('uniform_plate_tri_0.05.vtu')
    test_write_fe_results()
