================
Input file usage
================

--------------------------
Defining distributed loads
--------------------------

Input file keyword: ``dload``.

This option is used to define distributed loads to elements directly.

Each item in the list of distributed loads contains:

1. The external element number
2. The coordinate direction loaded (x, y, z, xy, xz, y, yz)
3. The magnitude of the load

Repeat this entry for every distributed load in the model.

-------
Example
-------

.. code-block:: yaml

    dload:
    - 1, x, 1000
    - 1, y, 1000
    - 2, xy, 700
    - 3, y, 1000
