===================
General Description
===================

Boundary conditions specify the node ID, boundary condition type, the degree of freedom being constrained, and the magnitude of the conditions.  The node ID is the *external* node number, the boundary condition type is 0 for Neumann and 1 for Dirchlet, the degree of freedom is 0 for :math:`x`, 1 for :math:`y`, and 2 for :math:`z`.  The magnitude is the value of the applied condition.
