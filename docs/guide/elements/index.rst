========
Elements
========

Elements are the fundamental entities in the Finite Element Method. They specify
the topological, constitutive and fabrication properties of a finite element
model.

.. rubric:: Contents

.. toctree::
   :maxdepth: 1

   description
   defining
   internal
   input
