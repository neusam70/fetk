=================
Defining elements
=================

Elements are defined in a table giving the nodes defining each element.  The
table contains ``num_elem`` sublist items:

.. code-block:: python

   elements = [element_0, element_1, . . ., element_n]

Each of the components of ``elements`` is a list defining the external element
ID and external node IDs.  The definition of internal element ``e`` is the eth
item in the ``elements`` table.

------------------------------
Definining individual elements
------------------------------

Individual elements are defined as a list containing 2 or more entries:

.. code-block:: python

   element = [ex, n1, ..., nn]


where ``ex`` is the external element number for the internal element ``e``; and
``n1``, ``n2``, to ``nn`` are the external node numbers of the nodes defining
the element.

.. rubric:: Example

.. _fig_elem_defn:

.. figure:: ./elem_defn.png
   :width: 4in

   Mesh for element definition example.

Consider the 9-node regular 2D mesh shown in Figure :ref:`fig_elem_defn`.  The
element table is given by

.. code-block:: python

   elements = [
       [1, 1, 2, 4, 5],
       [2, 4, 5, 8, 7],
       [3, 2, 3, 6, 5],
       [4, 5, 6, 9, 8],
   ]
