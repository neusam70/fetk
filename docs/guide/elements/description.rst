===================
General Description
===================

Each element has an external identifier, which is assigned by the user, and an
internal identifier, which is assigned by the program.

-----------------------
External identification
-----------------------

Elements are externally identified by an external element number.

The external element number is an integer in the range :math:`1 \ldots M`, where
:math:`M` is the maximum element number which may be assigned to an object. The
element sequence may have gaps. For example, suppose that six elements are
defined with numbers:

.. math::
   :label: ex_elem_1

   \mathbf{1}, \
   \mathbf{4}, \
   \mathbf{5}, \
   \mathbf{6}, \
   \mathbf{11}, \
   \mathbf{12}

There are two gaps: :math:`2-3`, and :math:`7-10`, in the sequence.

-----------------------
Internal identification
-----------------------

As each element is defined, it is assigned an internal element number. This is
an integer in the range :math:`0 \ldots N-1` where :math:`N` is the number of
elements.  There are no gaps. Internal element numbers are assigned once and
never change.  The internal number of an element numbering is defined implicitly by the order
in which it appears in the model's :ref:`element blocks<eb_index>`. Elements are
numbered internally (beginning with 0) consecutively across all element blocks.
For the previous example (assuming the elements belong to the same element block):

.. math::
   :label: ex_elem_2

   \begin{tabular}{lcccccc}
   \text{External:} & $\mathbf{1}$ & $\mathbf{4}$ & $\mathbf{5}$ & $\mathbf{6}$ & $\mathbf{11}$ & $\mathbf{12}$ \\
   \text{Internal:} & 0          & 1          & 2          & 3          & 4           & 5
   \end{tabular}

External and internal element numbers must be carefully distinguished. They only
coalesce when there are no gaps in the external element numbering and external
element numbers are assigned in ascending order, starting at zero.
