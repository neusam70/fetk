=============================
Organization of a FEM program
=============================

---------
Data flow
---------

The high level data flow in any FEM program is schematized in Figure
:ref:`fig_data_flow` This flows naturally suggests the grouping of data
structures into three classes:

Source Data Structures
  These bear a close relation to the FE model as the user defined it. For
  example, a table of node coordinates.

Computational Data Structures
  As the name suggests, these are organized with processing efficiency in
  mind. The use of arrays is important for this goal. Example are sparsely
  stored coefficient matrices, and partitioned solution vectors.

Result Data Structures
  These contain computed results again organized in a format that bears close
  relation to the FE model. Examples are completed node displacement vectors and
  element stress tables.

.. _fig_data_flow:

.. figure:: ./flow.png
   :width: 4in

   High level data flow in a finite element program

The feedback of results into source depicted in Figure :ref:`fig_data_flow`, is
one of the fundamental guides of the present study. The underlying philosophy is
to view results as data that, on return from the computational phase, completes
the unknown portions of source structures. This idea has unifying power because:

* It simplifies the merging of pre- and postprocessors so that the user deals with only one program.
* It is a natural way to organize saves and restarts in long remote computations.
* It allows a painless extension of linear into nonlinear analysis through a seamless cycle.

-------------------
Data flow breakdown
-------------------

Figure :ref:`fig_detailed_prog_breakdown` breaks down the data flow into
additional steps and identifies the program components that perform specific
functions. Some of these components deserve comment.

.. _fig_detailed_prog_breakdown:

.. figure:: ./detailed_prog_breakdown.png
   :width: 4in

   Further breakdown of data flow in FEM analysis system.

Commercial FEM systems usually distinguish between pre-processors, which define
the problem and carry out mesh generation functions, from post-processors, which
report and display results. This distinction has historical roots. The
separation has been eliminated in the present organization by feeding back all
results into source data structures. As a consequence there is no artificial
distinction between inputs and outputs at the leftmost end of Figure :ref:`fig_data_flow`.

The program component labeled "domain decomposer" is a fixture of task-parallel
parallel processing. Its function is to break down the model into subdomains
by element grouping. Usually each subdomain is assigned to a processor. Its
output is called partitioned source data, which is supplied as input to the
kernel. The results delivered by the kernel are usually partitioned by
subdomain, and must be reorganized through a reconstitution
process before being merged back into the source data.
