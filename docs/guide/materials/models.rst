.. _mat_models:

-------------------------
Avaliable material models
-------------------------

The list of available material models in ``fetk`` is sparse.

.. toctree::
   :maxdepth: 1

   elastic
