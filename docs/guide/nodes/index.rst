=====
Nodes
=====

Node points or nodes are selected space locations that serve two functions:

* define the geometry of the elements and hence that of the finite element model; and
* provide "resident locations" for the degrees of freedom. These freedoms specify the state of the finite element model.

.. rubric:: Contents

.. toctree::
   :maxdepth: 1

   description
   defining
   internal
   input
