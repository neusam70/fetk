.. _eb_internal:

=======================
Internal representation
=======================

Internally, element blocks are represented as a list of ``fetk.element.block.block`` objects.  Additionally, an external to internal element number mapping, ``elem_map``, is used to look up the internal element number given the external element number.

Defining element blocks, in particular element numbering, requires knowledge of every block and material in the model.  The function ``fetk.element.blocks.fe_objects`` will generate the list of element block objects and the element numbering map.

.. rubric:: Example 1

Consider the mesh shown in Figure :ref:`fig_elem_block_defn`.  Element blocks are generated via:

.. code-block:: python

    >>> import fetk.node
    >>> import fetk.element
    >>> import fetk.material
    >>> nodes = [
    ... [1, 0, 0],
    ... [2, 10, y1],
    ... [3, 10, 0],
    ... [4, 20, y2],
    ... [5, 20, 0],
    ... [6, 30, y3],
    ... [7, 30, 0],
    ... [8, 40, y2],
    ... [9, 40, 0],
    ... [10, 50, y1],
    ... [11, 50, 0],
    ... [12, 60, 0],
    ... ]
    >>> coords, node_map = fetk.node.fe_objects(nodes)
    >>> material = fetk.material.factory("elastic", 1, {"E": 10, "nu": .25})
    >>> element_blocks = [
    ...{
    ...    "name": "top longerons",
    ...    "material": 1,
    ...    "element type": "L2D2",
    ...    "element properties": {"area": 10.00},
    ...    "elements": [7, 8, 9, 10, 11, 12],
    ...},
    ...{
    ...    "name": "battens",
    ...    "material": 1,
    ...    "element type": "L2D2",
    ...    "element properties": {"area": 3.00},
    ...    "elements": [13, 14, 15, 16, 17],
    ...},
    ...{
    ...    "name": "diagonals",
    ...    "material": 1,
    ...    "element type": "L2D2",
    ...    "element properties": {"area": 1.00},
    ...    "elements": [18, 19, 20, 21],
    ...},
    ...{
    ...    "name": "bottom longerons",
    ...    "material": 1,
    ...    "element type": "L2D2",
    ...    "element properties": {"area": 2.00},
    ...    "elements": [1, 2, 3, 4, 5, 6],
    ...}]
    >>> element = fetk.element.factory("L2D2", {"area": 1}, material)
    >>> blocks = fetk.element.blocks.fe_objects(
    ...    element_blocks, elements, [material], node_map
    ... )
