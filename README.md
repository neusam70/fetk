# fetk

`fetk` is a python package providing modules and procedures for developing
finite element models.  The modules have been used in teaching the finite element method in a variety of introductory and advanced courses in the Department of Mechanical Engineering at the University of Utah.

## Install

No installaton necessary, simply source [`setup-env.sh`](./setup-env.sh) to put
`fetk` on your `PYTHONPATH`:

```console
source ./setup-env.sh
```

## Test

Tests are located in the [`test`](./test) directory and are designed to work
with `pytest`:

```console
cd test
py.test .
```

## Documentation

See the [guide](./docs/guide/index.rst) for more information.  The guide is a work in progress.
